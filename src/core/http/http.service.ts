/**
 * Http Service
 * This is just a basic implementation.
 * TODO: improve error handling and flexibility
 */
import { getItemFromLocalStorage } from '../auth/localstorage.helper';

export function getHeaders() {
  return {
    "Content-Type": "application/json",
    Authorization: getItemFromLocalStorage('token') || ''
  };
}

export interface IHttpResponse<T> extends Response {
  parsedBody?: T;
}


// =========================
// HTTP / Fetch utility
// =========================

export async function get<T>(url: string): Promise<T> {
  const response: IHttpResponse<T[]> = await fetch(url, {
    method: 'GET',
    headers: getHeaders()
  });
  return response.json();
}

export async function patch<T>(url: string, data: T): Promise<T>  {
  const response = await fetch(url,
    {
      method: 'PATCH',
      headers: getHeaders(),
      body: JSON.stringify(data)
    },
  );
  return response.json();
}

export async function post<T>(url: string, data?: any): Promise<T> {
  const params: any = {
    method: 'POST',
    headers: getHeaders()
  };
  if (data) {
    params.body = JSON.stringify(data);
  }
  const response = await fetch(url,params);
  return response.json();
}

export async function del<T>(url: string): Promise<T> {
  const response = await fetch(url,
    {
      method: 'DELETE',
      headers: getHeaders(),
    },
  );
  return response.json();
}
